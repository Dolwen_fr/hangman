﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hangman : MonoBehaviour {
	public InputField UserChoice;
	public Text Life;
	public Text WordToFind;
	public Text WordLen;
	public Text LettersUsed;
	public Image Hang;

	private string Word = "";
	private int Try = 5;
	private string WordLetterFound;

	void Start()
	{
		// mot a trouver
		Word = "Transversalite";

		// on force en minuscule
		Word = Word.ToLower ();

		for(int i = 0; i < Word.Length ; i++)
		{
			WordLetterFound += "_";
		}

		WordToFind.text = WordLetterFound;
		WordLen.text = "" + WordLetterFound.Length;
		LettersUsed.text = "";
		Life.text = Try + "/5";
	}

	public void StartGame()
	{
		// L'utilisateur a encore de la vie ?
		if (Try > 0)
		{
			// Recuperer la lettre saisie
			string tLetter = UserChoice.text.ToLower ();

			// Comparer la lettre saisie avec le mot a trouver
			bool isCorrect = Word.Contains (tLetter);

			// Ajouter la lettre utilisé
			LettersUsed.text += " " + tLetter;

			// Si erreur diminuer le nbr d'essai
			if (isCorrect == false) {
				Try = Try - 1;
				Life.text = Try + " / 5";

				// TODO Modifier l'image du pendu

			}

			if (isCorrect == true) {
				bool isFound = true;
				while (isFound) {
					// exemple avec : a
					// tIndex = 2
					int tIndex = Word.IndexOf (tLetter);

					if (tIndex >= 0) {
						// WordLetterFound = "_____________"
						WordLetterFound = WordLetterFound.Remove (tIndex, 1);
						// WordLetterFound = "__a___________"
						WordLetterFound = WordLetterFound.Insert (tIndex, tLetter);

						// Word = "Transversalite"
						Word = Word.Remove (tIndex, 1);
						// Word = "Tr_nsversalite"
						Word = Word.Insert (tIndex, "_");

						// Modifier l'interface
						WordToFind.text = WordLetterFound;
					} else {
						isFound = false;
					}

					// L'utilisateur a trouvé le mot
					bool tUnderscore = WordLetterFound.Contains("_");
					if (tUnderscore == false)
					{
						// User WIN !
					}
				}
			}
		}
	}	
}
